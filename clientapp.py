import mysql.connector
import sys
from datetime import datetime

books_db = mysql.connector.connect(
    host="localhost", user="root", password="root", database="test", port=3306
)
cursor = books_db.cursor()

def get_input(prompt="", table_name="", valid_values=None):
    if not prompt:
        return ""
    domain_values = []
    print(prompt)
    if table_name:
        cursor.execute(f"SELECT * FROM {table_name};")
        domain_values = [x[0] for x in cursor.fetchall()]
        valid_values = set([str(i + 1) for i in range(len(domain_values))])
    user_input = str(input("Please enter a value: "))

    if not valid_values:
        return user_input

    while (not user_input) or (
        user_input and user_input not in valid_values
    ):
        user_input = str(input("Please enter a valid value: "))

    if (not user_input) or not table_name:
        return user_input

    return domain_values[int(user_input) - 1]

def check_if_book_available():
    book_name = get_input(prompt="\nWhat is the name of the book you would like to checkout")
    cursor.execute(f"select * from checkoutData inner join SeattleInventory using (BibNumber) where Title like '%{book_name}%';")
    result1 = cursor.fetchone()
    if result1:
        print("Book with title: " + result1[4] + " is available, go back to the main menu to check it out!")
    else:
        print("Book not available")    

def checkout_book():
    book_name = get_input(prompt="\nWhat is the name of the book you would like to checkout")
    cursor.execute(f"select * from checkoutData inner join SeattleInventory using (BibNumber) where Title like '%{book_name}%';")
    cursor.fetchall()
    cursor.execute(f"select * from checkoutData inner join SeattleInventory using (BibNumber) where Title like '%{book_name}%';")
    result1 = cursor.fetchone()
    if result1:
        print("You will be checking out: " + result1[4])
        today = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        bib = result1[0]
        item = result1[1]
        collect = result1[2]
        print("Today's date is: " + today)
        cursor.fetchall()
        cursor.execute(f"INSERT INTO checkoutData(BibNumber, itemType, collection, checkOutDateTime) VALUES ({bib}, '{item}', '{collect}', '{str(today)}');")
        books_db.commit()
        cursor.execute(f"INSERT INTO checkoutHistory(BibNumber, itemType, collection, checkOutDateTime) VALUES ({bib}, '{item}', '{collect}', '{str(today)}');")
        books_db.commit()
        print("Book successfully checked out!")
    else:
        print("Could not checkout book")    
   

def get_ratings():
    book_name = get_input(prompt="\nWhat is the name of the book you would like to see ratings for")
    cursor.execute(f"SELECT bookName, rating FROM GoodreadBooks WHERE bookName LIKE '%{book_name}%';")
    result = cursor.fetchall()
    for subresult in result:
        print("Ratings for " + subresult[0] + " is " + str(subresult[1]) + "/5")

def get_reviews():
    book_name = get_input(prompt="\nWhat is the name of the book you would like to see user reviews for")
    cursor.execute(f"SELECT bookName, rating, userID FROM userReviews WHERE bookName LIKE '%{book_name}%';")
    result = cursor.fetchall()
    for subresult in result:
        if subresult[2]:
            print(subresult[0] + " Review: " + subresult[1])
            print(" reviewed by: " + str(subresult[2]))
        else:
            print(subresult[0] + " Review: " + subresult[1])
    

def set_review():
    book_name = get_input(prompt="\nWhat is the name of the book you would like to set a review for")
    if book_name:
        print("1. It was amazing")
        print("2. Really liked it")
        print("3. Liked it")
        print("4. It was ok")
        print("5. Did not like it")

        role_map = {
            "1": "it was amazing",
            "2": "really liked it",
            "3": "liked it",
            "4": "it was ok",
            "5": "did not like it",
        }

        rating = get_input(prompt="\nWhat is the review you would like to give this book?", valid_values=set([str(i) for i in range(1, 6)]))
        userID = get_input(prompt="\nWhat is the userID you will be reviewing under (must be an integer < 8 digits)?")
        cursor.execute(f"INSERT INTO userReviews(userID, bookName, rating) VALUES ({userID}, '{book_name}', '{role_map[rating]}');")
        books_db.commit()
        print("Review added!")

def query_rated_books_by_author():
    author_name = get_input(prompt="\nWhat is the name of the author who's work you are searching for?")
    if author_name:
        cursor.execute(f"SELECT bookName, author FROM GoodreadBooks WHERE author LIKE '%{author_name}%'")
        result = cursor.fetchall()
        print("Books written by " +  author_name + " are:")
        for subresult in result:
            print(subresult[0] + "\t\t\t\t" + subresult[1])

def query_books_by_name():
    book_name = get_input(prompt="\nWhat is the name of the book you are searching for?")
    cursor.execute(f"SELECT bookName, author, rating FROM GoodreadBooks WHERE bookName LIKE '%{book_name}%';")
    result = cursor.fetchall()
    print('Books matching {book_name} are:')
    print("Title:\t\t\t\t\t\t\t\tAuthor:\t\tRating:")
    for subresult in result:
        print(subresult[0] + "\t\t" + subresult[1] + "\t\t\t\t\t" + str(subresult[2]))

def get_checkout_history_by_name():
    book_name = get_input(prompt="\nWhat is the name of the book you are searching for?")
    cursor.execute(f"SELECT Title, checkOutDateTime FROM CheckoutHistory INNER JOIN SeattleInventory USING (BibNumber, itemType) WHERE Title like '%{book_name}%';")
    result = cursor.fetchall()
    print("Checkout history for " + book_name + " is the following:")
    for subresult in result:
        print("Title: " + subresult[0] + " checkoutDateTime: " + subresult[1])

def get_inventory_for_bookname():
    book_name = get_input(prompt="\nWhat is the name of the book you are searching for?")
    cursor.execute(f"select Title, itemLocation, itemcount from SeattleInventory where Title like '%{book_name}%'")
    result = cursor.fetchall()
    print("Inventory for " + book_name + " is the following:")
    for subresult in result:
        print("Title: " + subresult[0] + " Location: " + subresult[1])
    

while 1:
        option = 1000
        option = get_input(
                    prompt="""
What would you like to do?
1. Check out a book
2. Read ratings for a given book.
3. Review a book.
4. Find books written by a particular author.
5. Check if book by given name is available 
6. Query books by name.
7. Get reviews for a book title
8. Get checkout history by book name
9. Check inventory for book name

Pick one of the 10 options by entering the corresponding number""",
                    valid_values=set([str(i) for i in range(1, 10)]))
        
        if option == "1":
            checkout_book()
        elif option == "2":
            get_ratings()
        elif option == "3":
            set_review()
        elif option == "4":
            query_rated_books_by_author()
        elif option == "5":
            check_if_book_available()
        elif option == "6":
            query_books_by_name()
        elif option == "7":
            get_reviews()
        elif option == "8":
            get_checkout_history_by_name()
        elif option == "9":
            get_inventory_for_bookname()