
-- Code for 1st set of tables

create table GoodreadBooks (bookID decimal(10), bookName VARCHAR(50), author varchar(30), publisher VARCHAR(30), rating decimal(3), publishingYear YEAR(4), ISBN decimal(10), language char(10), numberOfPages decimal(5), numberOfRatings decimal(8), primary key(bookID));
create index GRBName on GoodreadBooks(bookName);
create index GRBAuthor on GoodreadBooks(author);

create table user (userID decimal(8), primary key(userID));



create table userReviews(userID decimal(8), bookName VARCHAR(50), rating varchar(35), primary key(userID,bookName), foreign key (bookName) references GoodreadBooks(bookName), foreign key (userID) references user(userID));
create index URBName on userReviews(bookName);

-- Code for 2nd set of tables

create table SeattleInventory (BibNumber decimal(10), Title varchar(50), Author varchar(50), PublicationYear varchar(20), Publisher varchar(50), subject varchar(50), itemType varchar(8), itemCollection varchar(8), itemLocation varchar(8), itemCount int, primary key(BibNumber, itemCollection, itemLocation));


create table checkoutData (BibNumber decimal(10), itemType varchar(8), collection varchar(8), checkOutDateTime varchar(25), primary key(BibNumber, checkOutDateTime), foreign key (BibNumber) references SeattleInventory(BibNumber));


create table checkoutHistory (BibNumber decimal(10), itemType varchar(8), collection varchar(8), checkOutDateTime varchar(25), primary key(BibNumber, checkOutDateTime), foreign key (BibNumber) references SeattleInventory(BibNumber));


create table dataDictionary (uniqueCodes varchar(8), description varchar(25), codeType varchar(10), formatGroup varchar(10), formatSubgroup varchar(10), primary key(uniqueCodes));
